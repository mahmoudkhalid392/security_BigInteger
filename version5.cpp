#include <vector>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>
#include <ctime>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <string>
#include <bits/stdc++.h>
#include <time.h>
using namespace std;
#define FOR(i,l,r) for (int i=l;i<=r;i++)
#define FORD(i,r,l) for (int i=r;i>=l;i--)
#define EL printf("\n")
const int base = 1000000000;
const int Blen=8;
class BigInt;
BigInt *mulandsub(BigInt *a,BigInt *b,int q,int j,int n);
string tostring(BigInt a);
vector <int>  add(vector<int>a,vector<int>b)
{
    vector<int>res;
    int carry = 0;
    for (int i = 0; i < max(a.size(),b.size()); ++i) {
        if(a.size() > i)carry += a[i];
        if(b.size() > i)carry += b[i];
        res.push_back(carry%base);
        carry /= base;
    }
    return res;
}
vector <int>  sub(vector<int>a,vector<int>b)
{
    vector<int>res;
    int carry = 0;
    for(int i = 0 ; i <= a.size()-1;i++)
    {
        carry += a[i] - (i < b.size() ? b[i] : 0);
        if (carry < 0) res.push_back(carry+base), carry = -1;
        else res.push_back(carry), carry = 0;
    }
    return res;
}
string binary(string x)
{
    for(int i=0;i<x.length();++i)
        x[i]-='0';
    string ans;
    while(1)
    {
        while(x.length()>0 && x[0]==0)
            x=x.substr(1,x.length());
        if(x.length()==0)
            return ans;
        if(x[x.length()-1]%2)
            ans=ans+"1";
        else
            ans=ans+"0";
        for(int i=0;i<x.length();++i)
        {
            if(x[i]%2 && i!=x.length()-1)
                x[i+1]+=10;
            x[i]/=2;
        }
    }
    return ans;
}
class BigInt
{
private :
    vector<int>data;
    bool sign;
public:
    void removezeros()
    {
        while (data.size() > 1 && data.back() == 0) data.pop_back();
    }
    BigInt()
    {
        sign = false;
    }

    BigInt(string s)
    {
        if (s.size() == 0)
        {
            data.push_back(0);
        }
        if(s[0]=='-')
        {
            sign = true; // -
            s.erase(0,1);
        }
        else sign = false; // +
        while (s.size()%9 != 0)
            s = '0'+s;
        for (int i=0;i<s.size();i+=9) {
            int v = 0;
            for (int j=i;j<i+9;j++){
                v = v*10+(s[j]-'0');
            }
            data.insert(data.begin(),v);
        }
        this->removezeros();
    }
    BigInt * dimmod(BigInt *a,int mod)
    {
        if( *this < a)
        {
            if(mod)
                return this;
            return new BigInt(0);
        }
        int d = base / (a->data.back() + 1 );
        int n = a->data.size();
        int m = data.size() - n;
        BigInt *u = *this * d;
        if(u->data.size() == m + n)
        {
            u->data.push_back(0);
        }
        BigInt *v = *a * d;
        BigInt *qou = new BigInt();
        qou->data.resize(m+1);
        int indexOfq = m;
        int j = u->data.size()-1;
        for( ;j >= n ; j-- )
        {
            long long q;
            if(u->data[j] == v->data.back())
                q = base-1;
            else
            {
                q = (u->data[j]);
                q *= base;
                q += u->data[j-1];
                q /=v->data.back();
            }
            if(!q)
            {
                indexOfq--;
                continue;
            }
            long long e1 = v->data[n-2];
            e1 *=  q;
            long long e2 = u->data[j];
            e2 *= base;
            e2 += u->data[j-1];
            e2 -=  q * v->data.back();
            e2 *= base;
            e2 += u->data[j-2];
            while( e1 > e2)
            {
                q--;
                long long e3 = v->data[n-2];
                e3 *= q;
                long long e4 = u->data[j];
                e4 *= base;
                e4 += u->data[j-1];
                e4 -=  q * v->data.back();
                e4 *= base;
                e4 += u->data[j-2];
                e1 = e3;
                e2 = e4;
            }
            // * and -
            BigInt *temp = mulandsub(u,v,q,j,n);
            if(temp->getSign())
            {
                q--;
                temp = *temp + v;
            }               
            int index = 0;
            int t ;
            for( t = j-n ; index  < temp->data.size(); t++)
            {
                u->data[t]=temp->data[index];
                index++;
            }
            for (; index < n+1 ; ++t) {
                u->data[t] = 0;
                index++;
            }
            qou->data[indexOfq] = q;
            indexOfq--;
        }
        if(mod)
        {
            BigInt *r = new BigInt();
            vector<int> o ;
            o.resize(n);
            int index = n-1;
            int count = n;
            for(int q = u->data.size() - m -2 ;count >= 1 ; )
            {
                o[index] = u->data[q];
                index--;
                q--;
                count--;
            }
            r->setData(o);
            r->removezeros();
            r = *r / d;
            return r;
        }
        qou->removezeros();
        return qou;
    }

    BigInt(int x) {
        if(x < 0 )
        {
            sign = true;
            x = x * -1;
        }
        if(x == 0)this->data.push_back(0);
        this->setSign(false);
        string s = "";
        while (x > 0) s = char(x%10+'0') + s, x /= 10;
        while (s.size()%9 != 0)
            s = '0'+s;
        for (int i=0;i<s.size();i+=9) {
            int v = 0;
            for (int j=i;j<i+9;j++){
                v = v*10+(s[j]-'0');
            }
            data.insert(data.begin(),v);
        }
        this->removezeros();
    }
    vector<int>getData()
    {
        return data;
    }
    void setData(vector<int> a)
    {
        data = a;
    }

    bool getSign()
    {
        return sign;
    }
    void setSign(bool x)
    {
        sign = x;
    }
    void print()
    {
        if (sign)
        {
            cout<<"-";
        }
        printf("%d", (data.size() == 0) ? 0 : data.back());
        FORD(i,data.size()-2,0) printf("%09d", data[i]);
    }

    BigInt *abs()
    {
        BigInt *abs = new BigInt();
        abs = this;
        abs->setSign(false);
        return abs;
    }

    bool operator > (BigInt *a)
    {
        removezeros();
        a->removezeros();
        if(sign == true && a->getSign() == false)
            return false;
        if(sign == false && a->getSign() == true)
            return true;
        if (a->data.size() > this->data.size())
        {
            return false;
        }else if(a->data.size() < this->data.size())
        {
            return true;
        }else{
            for (int i = a->data.size()-1; i >=0; --i)
            {
                if (a->data[i] > this->data[i])
                {
                    return false;
                }else if(a->data[i] < this->data[i]){
                    return true;
                }
            }
        }
        return false;
    }
    bool operator < (BigInt *a)
    {
        return !(*this > a);
    }
    bool operator == (BigInt *a)
    {
        removezeros();
        a->removezeros();
        if (this->data.size() > a->data.size() || this->data.size() < a->data.size() ||
                sign != a->getSign())
        {
            return false;
        }else{
            for (int i = a->data.size()-1; i >=0; --i)
            {
                if (a->data[i] != this->data[i])
                {
                    return false;
                }
            }
        }
        return true;
    }
    bool operator != (BigInt *a)
    {
        return !(*this ==  a);
    }
    bool operator <= (BigInt *a)
    {
        return (*this < a || *this == a);
    }
    bool operator >= (BigInt *a)
    {
        return (*this > a || *this == a);
    }
    bool operator < (int b) {
        BigInt *v = new BigInt(b);
        return (*this < v);
    }

    bool operator > (int b) {
        BigInt *v = new BigInt(b);
        return (*this > v);
    }

    bool operator == (int b) {
        BigInt *v = new BigInt(b);
        return (*this == v);
    }

    bool operator >= (int b) {
        BigInt *v = new BigInt(b);
        return (*this >= v);
    }

    bool operator <= (int b) {
        BigInt *v = new BigInt(b);
        return (*this <= v);
    }
    bool operator !=(int b)
    {
        BigInt *v = new BigInt(b);
        return (*this != v);
    }

    BigInt * operator - (BigInt *a)
    {
        removezeros();
        a->removezeros();
        BigInt *res ;
        a->setSign(!a->getSign());
        res = *this + a;
        return res;

    }
    BigInt * operator -(int b)
    {
        BigInt *v = new BigInt(b);
        return *this - v;
    }
    BigInt *operator + (BigInt *a)
    {
        removezeros();
        a->removezeros();
        BigInt *res = new BigInt();
        if(this == 0)return a;
        if(a == 0)return this;
        if(sign == a->getSign())
        {
            res->setSign(sign);
            res->setData(add(a->getData(),data));
        }else{
            if(this->sign == false)
            {
                if(*this->abs() >= a->abs())
                {
                    res->setSign(false);
                    res->setData(sub(data,a->getData()));
                }else{
                    res->setSign(true);
                    res->setData(sub(a->data,data));
                }
            }else{
                if(*this->abs() >= a->abs())
                {
                    res->setSign(true);
                    res->setData(sub(data,a->getData()));
                }else{
                    res->setSign(false);
                    res->setData(sub(a->data,data));
                }
            }
        }
        res->removezeros();
        return res;
    }
    BigInt *operator +(int b)
    {
        removezeros();
        BigInt * v = new BigInt(b);
        return *this + v;
    }

    BigInt *operator * (BigInt *a)
    {
        removezeros();
        a->removezeros();
        BigInt *ans = new BigInt();
        if(this->getSign() != a->getSign())
        {
            ans->setSign(true);
        }else{
            ans->setSign(false);
        }

        unsigned long long  carry = 0;
        ans->data.assign(a->data.size()+data.size(), 0);
        for(int i=0;i<=data.size()-1;i++){
            carry = 0ll;
            for (int j=0;j<a->data.size() || carry > 0;j++) {
                unsigned long long s = ans->data[i+j] + carry + (unsigned long long)data[i]*(j < a->data.size() ? (unsigned long long)a->data[j] : 0ll);
                ans->data[i+j] = s % base;
                carry = s / base;
            }
        }
        ans->removezeros();
        return ans;
    }
    BigInt *operator *(int b)
    {
        BigInt *v = new BigInt(b);
        return *this * v;
    }

    BigInt * operator / (BigInt *a) {
        if(a->data.size() > 1)
        {
            return dimmod(a,0);
        }
        long long d = a->data[0];
        return *this / d;
    }
    BigInt * operator /(int b)
    {
        BigInt *v = new BigInt();
        v->data.resize(data.size());
        long long cur = 0;
        for(int i = this->data.size()-1;i >= 0 ; i--)
        {
            cur = (cur % b) * base + data[i];
            v->data[i] = cur / b;
        }
        v->removezeros();
        return v;
    }

    BigInt  *operator % (BigInt *a) {
        if(a->data.size() > 1)
        {
            return this->dimmod(a,1);
        }
        long long d = a->data[0];
        return *this % d;

    }
    BigInt *operator %(int b)
    {
        BigInt *v = new BigInt ();
        long long cur = 0;
        for (int i = data.size()-1; i >= 0 ; --i) {
            cur *= base;
            cur += data[i];
            cur %= b;
        }
        v->data.push_back(cur);
        return v;
    }
    BigInt * square()
    {
        return *this * this;
    }
    BigInt * power3(string x,BigInt *mod)
    {
        BigInt *out = new BigInt(1);
        BigInt *temp = this;
        string ans=binary(x);
        for(int y = 0 ;y <ans.length() ; y++)
        {
            if(ans[y]=='1')
            {
                out =  *( *out * temp ) % mod;
            }
            temp =  *(*temp * temp) % mod;
        }
        return *out % mod ;
    }
    BigInt *power3(BigInt *a, BigInt*mod) {

        if( *a == 0)
        {
            BigInt *o = new BigInt(1);
           return o;
        }
        BigInt *temp = this->power3(*a/2, mod);
        temp = *temp % mod;
        if ( *(*a % 2 ) == 0)
            return *temp*temp;
        else {
            return *(*this * temp) * temp;
        }

    }
    BigInt * power(BigInt *a,BigInt *mod)
    {
        BigInt *out = new BigInt(1);
        BigInt *temp = this;
        string x = tostring(*a);
        string ans=binary(x);
        for(int y = 0 ;y <ans.length() ; y++)
        {
            if(ans[y]=='1')
            {
                out =  *( *out * temp ) % mod;
            }
            temp =  *(*temp * temp) % mod;
        }
        return *out % mod ;
    }

    string isprime(int iteration)
    {
        int i;
        BigInt *k = *this - 1;
        BigInt *s = k;
        if (*this < 2)
        {
            return "No";
        }
        if (*this != 2 && *(*this % 2) == 0)
        {
            return "No";
        }
        while ( *(*s % 2) == 0)
        {
            s = *s / 2;
        }
        for (i = 0; i < iteration; i++)
        {
            BigInt *a= new BigInt(rand());
            a =  *a % k ;
            a = *a + 1;
            BigInt *temp = s;
            BigInt * mod = a->power2(temp,this);
            while (*temp != k && *mod != 1 && *mod != k)
            {
                mod = mod->mulmod(mod,this);

                temp = *temp * 2;
            }
            if (*mod != k && *(*temp % 2) == 0)
            {
                return "No";
            }
        }
        return "Yes";
    }
    BigInt *mulmod(BigInt *a, BigInt *mod)
    {
        a = *a % mod;
        BigInt * out = *this % mod;
        out = *out * a;
        return *out % mod;
    }
    BigInt *gcd(BigInt *b) {
        BigInt *a = this;
        while (b > 0) {
            BigInt *r = *a%b;
            a = b;
            b = r;
        }
        return a;
    }
    BigInt * extendedEucluid(BigInt *m)
    {
        BigInt *A1 = new BigInt(1);
        BigInt *A2 = new BigInt(0);
        BigInt *A3 = m;
        BigInt *B1 = new BigInt(0);
        BigInt *B2 = new BigInt(1);
        BigInt *B3 = this;
L: if(*B3 == 0)return this->gcd(m);
        if(*B3 == 1)
        {
            if(B2->getSign() == true)
            {
                B2 = *B2 + m;
            }
            return B2;
        }
        BigInt *Q = *A3 / B3;
        BigInt *T1 = *A1 - *Q*B1;
        BigInt *T2 = *A2 - *Q*B2;
        BigInt  *T3= *A3 - *Q*B3;
        A1 =  B1;
        A2 = B2 ;
        A3 = B3;
        B1 = T1;
        B2 = T2 ;
        B3 = T3 ;
        goto L;


    }
    BigInt *power2(BigInt *a ,BigInt *mod)
    {
        BigInt *x = new BigInt(1);
        BigInt *y = this;
        while (*a > 0)
        {
            if (*(*a % 2 ) == 1)
            {
                x = *(*x * y) % mod;
            }
            y = *(*y * y) % mod;
            a = *a / 2;
        }
        return *x % mod;
    }
   
};
BigInt *phiofn(BigInt *p,BigInt *q)
{
    return *(*p - 1) * (*q - 1);
}
string tostring(BigInt a)
{
    a.removezeros();
    string out = "";
    vector <int> aa = a.getData();
    out += to_string(aa[aa.size()-1]);
    for (int i = aa.size()-2; i >= 1;--i) {
        string h ="";
        if(aa[i]==0)
        {
           h += "000000000";
        }else{
            h += to_string(aa[i]);
        }
        while(h.size() < 9)
           h = "0" + h;
        out += h;
    }
    if(aa.size() > 1)
         out += to_string(aa[0]);
    return out;
}
void program()
{
    string input1;
    cin >> input1;
    string input2;
    cin >> input2;
    string input3;
    cin >> input3;
    string p1 = input1.erase(0,2);
    string q1 = input2.erase(0,2);
    string e1 = input3.erase(0,2);
    BigInt *p = new BigInt(p1);
    BigInt *q = new BigInt(q1);
    BigInt *e = new BigInt(e1);
    BigInt *pn = phiofn(p,q);
    BigInt *n = *p*q;
    string operation;
    cin >> operation;
    while (operation != "Quit") {
        if(operation == "IsPPrime")
        {
            cout<< p->isprime(1) <<endl;
        }else if(operation =="IsQPrime")
        {
            cout << q->isprime(1) << endl;
        }else if(operation == "PrintN")
        {
            n->print();cout<<endl;
        }else if(operation =="PrintD")
        {
            BigInt *d = e->extendedEucluid(pn);
            d->print();cout<<endl;
        }else if(operation == "PrintPhi")
        {
            pn->print();cout<<endl;
        }else if(operation.substr(0,13) == "EncryptPublic")
        {
            string m = operation.substr(15,-1);
            m.pop_back();
            BigInt *message = new BigInt(m);
            BigInt *out = message->power(e,n);
            out->print();cout<<endl;
        }else if(operation.substr(0,14) == "EncryptPrivate")
        {
            BigInt *d = e->extendedEucluid(pn);
            string m = operation.substr(16,-1);
            m.pop_back();
            BigInt *message = new BigInt(m);
            BigInt *out =  message->power(d,n);
            out->print();cout<<endl;
        }else if(operation == "Quit")
        {
            break;
        }
        cin >> operation;
    }

}
BigInt * mulandsub(BigInt *a,BigInt *b,int q,int j,int n)
{
    BigInt * temp = *b * q;
    BigInt *temp1=new BigInt();
    vector<int>subu;
    subu.resize(n+1);
    int iu = n;
    for(int o = j ; o >= j - n ; o--)
    {
        subu[iu] = a->getData()[o];
        iu--;
    }
    temp1->setData(subu);
    BigInt * out = new BigInt();
    out = *temp1 - temp;
    return out;
}

int main()
{
    freopen("i","r",stdin);
    //BigInt *p = new BigInt("4397678428390379126255360246165021910057442267382175543246817108158797115317154540746718616555865161372450860559307149988169566508274711121236049281217144195628407516579953387138808449458611836421032431582081899650685651973204503916459595600207918950383877057152533041873901965623112895996177941667469292738");
    //BigInt *q = new BigInt("25051719899710729069339146813050963409059898810366373119834423967819636191509401691818253978210229371822961344590338934536803264841097247978074700319812702399440521918349189245279566231685265955731649745935378380489722580113725907099133943430294137060596724659637599737926649148356615085679203385772673944833");
    //BigInt *o = new BigInt();
     //BigInt *n = new BigInt("25548364798832019218170326077010425733930233389897468141147917831084690989884562791601588954296621731652139141347541240725432606132471100644835778517336041031200174441223836394229943651678525471050219216183727749114047330431603023948126844573697946795476319956787513765533596926704755530772983549787878951983");
      //o = p->power(q,n);
      //o->print();cout<<endl;
     program();
    return 0;
}

